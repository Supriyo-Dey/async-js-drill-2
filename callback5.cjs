/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const dataBoards = require("./data/boards_1.json");

function callback5(name, power1, power2) {

    setTimeout(() => {

        const { controlBoard } = require("./callback1.cjs");
        const { controlList } = require("./callback2.cjs");
        const { controlCard } = require("./callback3.cjs");

        const board = dataBoards.find(element => element.name === name);
        const boardId = board.id;

        controlBoard(boardId, (dataBoard) => {
            console.log(dataBoard);

            controlList(boardId, (dataList) => {

                console.log(dataList);

                const mind = dataList.find(element => element.name === power1)
                const mindId = mind.id;

                const space = dataList.find(element => element.name === power2)
                const spaceId = space.id;

                controlCard(boardId, (dataCard) => {
                    console.log(dataCard[mindId]);
                    console.log(dataCard[spaceId]);
                })
            })
        });
    }, 2 * 1000);
}

module.exports = { callback5 };