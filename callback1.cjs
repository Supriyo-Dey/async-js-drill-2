/* 
	Problem 1: Write a function that will return a particular board's information 
    based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

const data = require("./data/boards_1.json")


function controlBoard(id,callback){
    setTimeout(()=>{
        const result = data.filter(element => element.id === id);
        callback(result[0]);
    },2*1000);
}

module.exports = {controlBoard};
