/* 
    Problem 3: Write a function that will return all 
    cards that belong to a particular list based on the 
    listID that is passed to it from the given data in 
    cards.json. Then pass controlCard back to the code that 
    called it by using a callback function.
*/

const dataCards = require("./data/cards_1.json");
const dataLists = require("./data/lists_1.json");

function controlCard(id, callback) {
    setTimeout(() => {
        const data = {}
        const listItem = dataLists[id];
        for (let item of listItem) {
            if(dataCards[item.id]) {
                data[item.id] = dataCards[item.id]
            } 
        }

        callback(data);
    }, 2 * 1000);
}

module.exports = { controlCard };