/* 
	Problem 2: Write a function that will return all lists 
    that belong to a board based on the boardID
    that is passed to it from the given data in lists.json.
    Then pass List back to the code that called it by using a callback function.
*/

const data = require("./data/lists_1.json");
// console.log(data);


function controlList(id,callback){
    setTimeout(()=>{
        
        for(let index in data){
            if(index === id){
                callback(data[index]);
            }
        }

    },2*1000);
};

module.exports = {controlList};